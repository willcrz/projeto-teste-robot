* Settings *
#Serve para importar as bibliotecas que serão usadas nos teste

Resource		${EXECDIR}/resources/base.robot

Test Setup		Start Session
Test Teardown	Take Screenshot

* Test Cases *
# Teste a serem executados
Deve buscar um único restaurante

	Go To restaurants
	Search By						DEBUGER
	Restaurant Should Be visible	DEBUGER KING
	Restaurant Count Should Be  	1
	
	#Think time --> Espera todos os elementos visuais serem carregados para gerar o print
	#Sleep	1
	

Deve buscar por categoria

	Go To restaurants
	Search By	CAFE
	Restaurant Should Be visible	STARBUGS COFFEE	

	
Deve buscar todos os restaurantes

	Go To restaurants
	Search By					a
	Restaurant Count Should Be	5

	
	#Serve como contadr para esperar carregar os dados da api para assim validar
	#Wait For Elements State		css=div[class="place-info-box"][style="opacity: 1;"]	visible

	#Think time --> Espera todos os elementos visuais serem carregados para gerar o print
	#Sleep	1

 


	