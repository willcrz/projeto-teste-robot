* Settings *
#Serve para importar as bibliotecas que serão usadas nos teste

Resource		${EXECDIR}/resources/base.robot

Test Setup		Start Session
Test Teardown	Take Screenshot

* Test Cases *
Deve adicionar um item ao carrinho

    &{restaurant}   Create Dictionary   restaurant= STARBUGS COFFE        desc=Nada melhor que um café pra te ajudar a resolver um bug.

    Go To restaurants
    Choose Restaurant   ${restaurant}

    Add To cart                 Starbugs 500 error
    Should Add To Cart          Starbugs 500 error
    Total Cart Should Be        15,60

Deve adicionar 3 itens ao carrinho
    [tags]      temp

    ${cart_json}        Get JSON    cart.json
    Go To restaurants
    Choose Restaurant   ${cart_json}

    FOR     ${products}  IN  @{cart_json["products"]}
            Add To cart         ${products["name"]}
            Should Add To Cart  ${products["name"]}
    END

    Total Cart Should Be    ${cart_json["total"]}   
