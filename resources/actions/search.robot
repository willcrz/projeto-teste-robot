* Settings *
Documentation       Ações da funcionalidade de busca de restaurantes

* Variables *	
${DIV_BOX_RESTAURANT}	css=div[class="place-info-box"][style="opacity: 1;"]		

* Keywords *
Go To restaurants

	Click			text= Estou com fome!
	Get Text		h1 strong	contains	Ta na hora de matar a fome!

Search By

	[Arguments]		${value}

	Click			css=.search-link

	#Serve para fazer o preenchimento do campo
	Fill Text		css=input[formcontrolname="searchControl"]	${value}

Choose Restaurant
        [Arguments]     ${super_var}

        Click   text= ${super_var["restaurant"]}

        Wait For Elements State   css=#detail     visible
        Get text    css=#detail     contains    ${super_var["desc"]}

Restaurant Should Be visible

	[Arguments]		${name}

	#Serve como contadr para esperar carregar os dados da api para assim validar
	Wait For Elements State		${DIV_BOX_RESTAURANT}	visible

	#Serve como uma check point de verificação para validar a tela
	Get Text					${DIV_BOX_RESTAURANT}	contains	${name}

Restaurant Count Should Be
	[Arguments]		${qtd}

	Wait For Elements State		${DIV_BOX_RESTAURANT}			visible
	Get Element Count			css=.place-info-box	equal		${qtd}