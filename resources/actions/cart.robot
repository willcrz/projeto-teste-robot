* Settings *
Documentation       Ações da funcionalidade do carrinho de compras

* Keywords *
Add To cart
    [Arguments]     ${name}

    Click   xpath=//span[text()="${name}"]/../a[@class="add-to-cart"]

Should Add To Cart 
    [Arguments]         ${name}

    Wait For Elements State     css=#cart tr >> text=${name}        visible

    Sleep   2

Total Cart Should Be
    [Arguments]         ${total}

    Log                 ${total}
    Get Text            xpath =//th[contains(text(),"Total")]/..//td    contains    ${total}

